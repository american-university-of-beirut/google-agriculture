Total Number of rare cases: 16 out of 1092
Percentage of Rare Cases: 1.47%

number of rare in train: 11/761
==> 1.445466491458607%%
number of rare in test: 5/331
==> 1.5105740181268883%%
The size of the original data is (761, 44)
The size of the oversampled data is (763, 44)
Total Number of rare cases: 14 out of 763
Percentage of Rare Cases: 1.83%

The percentage of rare values in dataset after smogn are 1.834862385321101
