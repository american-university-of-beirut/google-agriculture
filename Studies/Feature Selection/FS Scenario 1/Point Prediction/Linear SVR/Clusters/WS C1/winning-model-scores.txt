
Utility Based Metrics
F1: 0.92789
F2: 0.89266
F05: 0.96601
precision: 0.99322
recall: 0.87062

Regression Error Metrics
R2: 0.42097
Adj-R2: 0.40944
MSE: 3.18400
MAE: 1.32875
MAPE: 45.90649

Correlations
Pearson: 0.64888
Spearman: 0.65343
Distance: 0.35112
