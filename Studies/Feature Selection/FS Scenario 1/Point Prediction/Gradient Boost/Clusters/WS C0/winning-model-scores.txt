
Utility Based Metrics
F1: 0.93522
F2: 0.90632
F05: 0.96603
precision: 0.98772
recall: 0.88803

Regression Error Metrics
R2: 0.39470
Adj-R2: 0.37087
MSE: 2.65468
MAE: 1.18450
MAPE: 46.68585

Correlations
Pearson: 0.63477
Spearman: 0.53301
Distance: 0.36523
