Total Number of rare cases: 584 out of 3386
Percentage of Rare Cases: 17.25%

number of rare in train: 407/2361
==> 17.238458280389665%%
number of rare in test: 177/1025
==> 17.268292682926827%%
The size of the original data is (2361, 20)
The size of the oversampled data is (2442, 20)
Total Number of rare cases: 489 out of 2442
Percentage of Rare Cases: 20.02%

The percentage of rare values in dataset after smogn are 20.024570024570025
