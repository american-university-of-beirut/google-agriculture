
Utility Based Metrics
F1: 0.93276
F2: 0.91096
F05: 0.95562
precision: 0.97149
recall: 0.89699

Regression Error Metrics
R2: 0.47407
Adj-R2: 0.46577
MSE: 2.48501
MAE: 1.19603
MAPE: 42.42903

Correlations
Pearson: 0.69134
Spearman: 0.66342
Distance: 0.30866

Utility Based Metrics
F1: 0.93851
F2: 0.90981
F05: 0.96908
precision: 0.99059
recall: 0.89164

Regression Error Metrics
R2: 0.46993
Adj-R2: 0.46299
MSE: 2.57083
MAE: 1.22078
MAPE: 43.68547

Correlations
Pearson: 0.68779
Spearman: 0.65458
Distance: 0.31221
