
Utility Based Metrics
F1: 0.92422
F2: 0.90779
F05: 0.94125
precision: 0.95296
recall: 0.89716

Regression Error Metrics
R2: 0.13248
Adj-R2: 0.12283
MSE: 4.24433
MAE: 1.68686
MAPE: 67.88931

Correlations
Pearson: 0.54486
Spearman: 0.52861
Distance: 0.45514
