
Utility Based Metrics
F1: 0.00001
F2: 0.00001
F05: 0.00001
precision: 0.00001
recall: 0.00001

Regression Error Metrics
R2: 0.60812
Adj-R2: 0.40772
RMSE: 0.90325
MSE: 0.81586
MAE: 0.72972
MAPE: 23.01586

Correlations
Pearson: 0.86194
Spearman: 0.84916
Distance: 0.13806
