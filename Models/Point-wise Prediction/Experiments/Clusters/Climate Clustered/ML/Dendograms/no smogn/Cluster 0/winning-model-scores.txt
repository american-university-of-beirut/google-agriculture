
Utility Based Metrics
F1: 0.81415
F2: 0.81064
F05: 0.81769
precision: 0.82007
recall: 0.80831

Regression Error Metrics
R2: 0.48559
Adj-R2: 0.45447
RMSE: 1.15973
MSE: 1.34497
MAE: 0.89552
MAPE: 37.73203

Correlations
Pearson: 0.74661
Spearman: 0.72852
Distance: 0.25339
