
Utility Based Metrics
F1: 0.00002
F2: 0.00005
F05: 0.00001
precision: 0.00001
recall: 0.64332

Regression Error Metrics
R2: 0.52020
Adj-R2: 0.48648
RMSE: 1.46810
MSE: 2.15532
MAE: 1.08643
MAPE: 32.60589

Correlations
Pearson: 0.72310
Spearman: 0.75576
Distance: 0.27690
