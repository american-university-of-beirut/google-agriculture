
Utility Based Metrics
F1: 0.65551
F2: 0.64993
F05: 0.66117
precision: 0.66501
recall: 0.64627

Regression Error Metrics
R2: 0.25585
Adj-R2: 0.15964
RMSE: 1.12767
MSE: 1.27163
MAE: 0.79213
MAPE: 35.54067

Correlations
Pearson: 0.52552
Spearman: 0.55910
Distance: 0.47448
