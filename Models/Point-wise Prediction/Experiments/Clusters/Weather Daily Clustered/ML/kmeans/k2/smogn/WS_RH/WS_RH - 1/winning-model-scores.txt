
Utility Based Metrics
F1: 0.78098
F2: 0.75827
F05: 0.80508
precision: 0.82200
recall: 0.74385

Regression Error Metrics
R2: 0.46685
Adj-R2: 0.44266
RMSE: 1.46821
MSE: 2.15564
MAE: 1.09641
MAPE: 40.27229

Correlations
Pearson: 0.70252
Spearman: 0.73567
Distance: 0.29748
