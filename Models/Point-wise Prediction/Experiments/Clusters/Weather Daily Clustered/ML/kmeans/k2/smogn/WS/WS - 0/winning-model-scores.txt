
Utility Based Metrics
F1: 0.73568
F2: 0.74266
F05: 0.72883
precision: 0.72434
recall: 0.74738

Regression Error Metrics
R2: 0.55894
Adj-R2: 0.54406
RMSE: 1.29240
MSE: 1.67029
MAE: 0.96781
MAPE: 36.15608

Correlations
Pearson: 0.75776
Spearman: 0.76666
Distance: 0.24224
