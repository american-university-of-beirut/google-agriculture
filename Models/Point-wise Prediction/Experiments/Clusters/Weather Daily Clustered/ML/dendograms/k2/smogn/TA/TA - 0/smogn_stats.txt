Total Number of rare cases: 1316 out of 9428
Percentage of Rare Cases: 13.96%

number of rare in train: 1052/7536
==> 13.959660297239914%%
number of rare in test: 264/1892
==> 13.953488372093023%%
The size of the original data is (7536, 49)
The size of the oversampled data is (7760, 49)
Total Number of rare cases: 1349 out of 7760
Percentage of Rare Cases: 17.38%

The percentage of rare values in dataset after smogn are 17.3840206185567
