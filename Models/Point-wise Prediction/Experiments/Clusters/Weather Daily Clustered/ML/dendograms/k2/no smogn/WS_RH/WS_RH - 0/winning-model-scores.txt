
Utility Based Metrics
F1: 0.78739
F2: 0.76659
F05: 0.80934
precision: 0.82468
recall: 0.75332

Regression Error Metrics
R2: 0.50338
Adj-R2: 0.48114
RMSE: 1.40524
MSE: 1.97471
MAE: 1.04632
MAPE: 38.49088

Correlations
Pearson: 0.72563
Spearman: 0.75652
Distance: 0.27437
