Total Number of rare cases: 1069 out of 8497
Percentage of Rare Cases: 12.58%

number of rare in train: 854/6787
==> 12.582879033446295%%
number of rare in test: 215/1710
==> 12.573099415204677%%
The size of the original data is (6787, 49)
The size of the oversampled data is (6965, 49)
Total Number of rare cases: 1073 out of 6965
Percentage of Rare Cases: 15.41%

The percentage of rare values in dataset after smogn are 15.40559942569993
