Total Number of rare cases: 761 out of 5656
Percentage of Rare Cases: 13.45%

number of rare in train: 607/4512
==> 13.453014184397164%%
number of rare in test: 154/1144
==> 13.461538461538462%%
The size of the original data is (4512, 49)
The size of the oversampled data is (4640, 49)
Total Number of rare cases: 769 out of 4640
Percentage of Rare Cases: 16.57%

The percentage of rare values in dataset after smogn are 16.573275862068964
