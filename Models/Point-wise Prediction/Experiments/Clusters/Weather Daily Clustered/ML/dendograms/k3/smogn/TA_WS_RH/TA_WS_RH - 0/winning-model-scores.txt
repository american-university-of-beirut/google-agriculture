
Utility Based Metrics
F1: 0.73105
F2: 0.71666
F05: 0.74603
precision: 0.75636
recall: 0.70738

Regression Error Metrics
R2: 0.47873
Adj-R2: 0.45615
RMSE: 1.43439
MSE: 2.05747
MAE: 1.08732
MAPE: 37.66372

Correlations
Pearson: 0.70909
Spearman: 0.75423
Distance: 0.29091
