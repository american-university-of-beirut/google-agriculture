
Utility Based Metrics
F1: 0.79042
F2: 0.74394
F05: 0.84310
precision: 0.88231
recall: 0.71587

Regression Error Metrics
R2: 0.38168
Adj-R2: 0.33978
RMSE: 1.42748
MSE: 2.03769
MAE: 1.07448
MAPE: 38.42462

Correlations
Pearson: 0.64032
Spearman: 0.66920
Distance: 0.35968
