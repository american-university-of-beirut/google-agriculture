Total Number of rare cases: 86 out of 1641
Percentage of Rare Cases: 5.24%

number of rare in train: 69/1311
==> 5.263157894736842%%
number of rare in test: 17/330
==> 5.151515151515151%%
The size of the original data is (1311, 47)
The size of the oversampled data is (1326, 47)
Total Number of rare cases: 93 out of 1326
Percentage of Rare Cases: 7.01%

The percentage of rare values in dataset after smogn are 7.013574660633484
