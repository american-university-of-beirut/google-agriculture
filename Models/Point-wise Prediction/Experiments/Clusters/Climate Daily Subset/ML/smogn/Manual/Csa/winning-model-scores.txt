
Utility Based Metrics
F1: 0.78770
F2: 0.80171
F05: 0.77418
precision: 0.76542
recall: 0.81132

Regression Error Metrics
R2: 0.46291
Adj-R2: 0.42343
RMSE: 1.19076
MSE: 1.41790
MAE: 0.94700
MAPE: 36.23942

Correlations
Pearson: 0.74012
Spearman: 0.72152
Distance: 0.25988
