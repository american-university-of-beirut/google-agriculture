
Utility Based Metrics
F1: 0.75461
F2: 0.73811
F05: 0.77187
precision: 0.78381
recall: 0.72751

Regression Error Metrics
R2: 0.52479
Adj-R2: 0.50391
RMSE: 1.26216
MSE: 1.59304
MAE: 0.93979
MAPE: 33.31086

Correlations
Pearson: 0.73123
Spearman: 0.75282
Distance: 0.26877

Utility Based Metrics
F1: 0.77877
F2: 0.75155
F05: 0.80804
precision: 0.82880
recall: 0.73443

Regression Error Metrics
R2: 0.52371
Adj-R2: 0.50278
RMSE: 1.26358
MSE: 1.59664
MAE: 0.94438
MAPE: 33.45202

Correlations
Pearson: 0.73138
Spearman: 0.74977
Distance: 0.26862
