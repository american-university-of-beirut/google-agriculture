
Utility Based Metrics
F1: 0.60067
F2: 0.61134
F05: 0.59036
precision: 0.58368
recall: 0.61867

Regression Error Metrics
R2: 0.20143
Adj-R2: 0.04858
RMSE: 0.96318
MSE: 0.92771
MAE: 0.67390
MAPE: 32.89903

Correlations
Pearson: 0.45860
Spearman: 0.48891
Distance: 0.54140

Utility Based Metrics
F1: 0.69852
F2: 0.70619
F05: 0.69103
precision: 0.68612
recall: 0.71139

Regression Error Metrics
R2: 0.45602
Adj-R2: 0.43836
RMSE: 1.40037
MSE: 1.96104
MAE: 1.06114
MAPE: 36.79170

Correlations
Pearson: 0.69524
Spearman: 0.73098
Distance: 0.30476
