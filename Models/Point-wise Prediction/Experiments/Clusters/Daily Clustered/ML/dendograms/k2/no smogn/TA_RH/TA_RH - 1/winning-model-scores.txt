
Utility Based Metrics
F1: 0.74638
F2: 0.69505
F05: 0.80589
precision: 0.85113
recall: 0.66459

Regression Error Metrics
R2: 0.38830
Adj-R2: 0.33741
RMSE: 1.12357
MSE: 1.26240
MAE: 0.79828
MAPE: 34.54189

Correlations
Pearson: 0.62482
Spearman: 0.65092
Distance: 0.37518
