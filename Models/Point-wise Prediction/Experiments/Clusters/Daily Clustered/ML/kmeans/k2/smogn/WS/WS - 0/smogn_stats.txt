Total Number of rare cases: 916 out of 7453
Percentage of Rare Cases: 12.29%

number of rare in train: 731/5951
==> 12.283649806755168%%
number of rare in test: 185/1502
==> 12.316910785619175%%
The size of the original data is (5951, 49)
The size of the oversampled data is (6102, 49)
Total Number of rare cases: 911 out of 6102
Percentage of Rare Cases: 14.93%

The percentage of rare values in dataset after smogn are 14.929531301212718
