
Utility Based Metrics
F1: 0.79182
F2: 0.75192
F05: 0.83619
precision: 0.86865
recall: 0.72748

Regression Error Metrics
R2: 0.54919
Adj-R2: 0.52500
RMSE: 1.42614
MSE: 2.03387
MAE: 1.06008
MAPE: 38.28475

Correlations
Pearson: 0.74956
Spearman: 0.78204
Distance: 0.25044
