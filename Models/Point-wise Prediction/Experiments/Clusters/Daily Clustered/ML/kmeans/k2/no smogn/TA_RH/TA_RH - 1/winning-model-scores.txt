
Utility Based Metrics
F1: 0.63870
F2: 0.64960
F05: 0.62816
precision: 0.62133
recall: 0.65707

Regression Error Metrics
R2: 0.35997
Adj-R2: 0.30012
RMSE: 1.15461
MSE: 1.33312
MAE: 0.82025
MAPE: 35.60910

Correlations
Pearson: 0.60046
Spearman: 0.61776
Distance: 0.39954
