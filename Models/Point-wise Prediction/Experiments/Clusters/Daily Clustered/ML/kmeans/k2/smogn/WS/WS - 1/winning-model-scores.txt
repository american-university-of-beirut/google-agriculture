
Utility Based Metrics
F1: 0.73573
F2: 0.70147
F05: 0.77351
precision: 0.80093
recall: 0.68034

Regression Error Metrics
R2: 0.36119
Adj-R2: 0.31304
RMSE: 1.47631
MSE: 2.17949
MAE: 1.08183
MAPE: 37.77182

Correlations
Pearson: 0.62760
Spearman: 0.68047
Distance: 0.37240
