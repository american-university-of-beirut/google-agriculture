
Utility Based Metrics
F1: 0.71819
F2: 0.72206
F05: 0.71437
precision: 0.71184
recall: 0.72466

Regression Error Metrics
R2: 0.55610
Adj-R2: 0.53476
RMSE: 1.24421
MSE: 1.54806
MAE: 0.92934
MAPE: 32.89140

Correlations
Pearson: 0.75122
Spearman: 0.76650
Distance: 0.24878
