
Utility Based Metrics
F1: 0.75436
F2: 0.70932
F05: 0.80551
precision: 0.84365
recall: 0.68217

Regression Error Metrics
R2: 0.23091
Adj-R2: 0.00471
RMSE: 1.11019
MSE: 1.23252
MAE: 0.80744
MAPE: 33.56144

Correlations
Pearson: 0.51832
Spearman: 0.48341
Distance: 0.48168
