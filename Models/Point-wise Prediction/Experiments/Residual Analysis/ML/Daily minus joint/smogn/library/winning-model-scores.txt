
Utility Based Metrics
F1: 0.65090
F2: 0.69664
F05: 0.61079
precision: 0.58669
recall: 0.73088

Regression Error Metrics
R2: 0.24192
Adj-R2: 0.15432
RMSE: 1.92975
MSE: 3.72395
MAE: 1.41426
MAPE: 46.64979

Correlations
Pearson: 0.57771
Spearman: 0.63177
Distance: 0.42229
