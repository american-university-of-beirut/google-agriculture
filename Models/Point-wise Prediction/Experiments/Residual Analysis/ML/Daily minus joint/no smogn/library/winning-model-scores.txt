
Utility Based Metrics
F1: 0.72488
F2: 0.71298
F05: 0.73719
precision: 0.74563
recall: 0.70526

Regression Error Metrics
R2: 0.31012
Adj-R2: 0.24512
RMSE: 1.47522
MSE: 2.17627
MAE: 1.09993
MAPE: 39.82953

Correlations
Pearson: 0.58768
Spearman: 0.62923
Distance: 0.41232

Utility Based Metrics
F1: 0.68640
F2: 0.70042
F05: 0.67294
precision: 0.66426
recall: 0.71008

Regression Error Metrics
R2: 0.34369
Adj-R2: 0.26784
RMSE: 1.79556
MSE: 3.22403
MAE: 1.36986
MAPE: 45.62446

Correlations
Pearson: 0.60798
Spearman: 0.63966
Distance: 0.39202
