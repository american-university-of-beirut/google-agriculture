
Utility Based Metrics
F1: 0.79402
F2: 0.78806
F05: 0.80007
precision: 0.80416
recall: 0.78414

Regression Error Metrics
R2: 0.59097
Adj-R2: 0.57014
RMSE: 1.45918
MSE: 2.12921
MAE: 1.07356
MAPE: 36.30949

Correlations
Pearson: 0.76926
Spearman: 0.75653
Distance: 0.23074
