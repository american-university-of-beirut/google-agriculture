
Utility Based Metrics
F1: 0.81663
F2: 0.79511
F05: 0.83934
precision: 0.85520
recall: 0.78138

Regression Error Metrics
R2: 0.62009
Adj-R2: 0.60115
RMSE: 1.40628
MSE: 1.97762
MAE: 1.02929
MAPE: 34.13693

Correlations
Pearson: 0.78772
Spearman: 0.76056
Distance: 0.21228
