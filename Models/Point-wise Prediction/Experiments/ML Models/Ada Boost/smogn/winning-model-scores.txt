
Utility Based Metrics
F1: 0.00002
F2: 0.00005
F05: 0.00001
precision: 0.00001
recall: 0.71199

Regression Error Metrics
R2: 0.43249
Adj-R2: 0.40420
RMSE: 1.71876
MSE: 2.95415
MAE: 1.31049
MAPE: 46.80726

Correlations
Pearson: 0.66108
Spearman: 0.64508
Distance: 0.33892
