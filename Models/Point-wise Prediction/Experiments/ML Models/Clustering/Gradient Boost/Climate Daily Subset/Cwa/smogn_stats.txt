Total Number of rare cases: 836 out of 5123
Percentage of Rare Cases: 16.32%

number of rare in train: 667/4090
==> 16.3080684596577%%
number of rare in test: 169/1033
==> 16.36011616650532%%
Total Number of rare cases: 63 out of 196
Percentage of Rare Cases: 32.14%

number of rare in train: 50/156
==> 32.05128205128205%%
number of rare in test: 13/40
==> 32.5%%
The size of the original data is (156, 45)
The size of the oversampled data is (166, 45)
Total Number of rare cases: 63 out of 166
Percentage of Rare Cases: 37.95%

The percentage of rare values in dataset after smogn are 37.95180722891566
