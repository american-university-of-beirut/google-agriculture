
Utility Based Metrics
F1: 0.81707
F2: 0.79283
F05: 0.84284
precision: 0.86094
recall: 0.77745

Regression Error Metrics
R2: 0.39241
Adj-R2: 0.32909
MSE: 1.24480
MAE: 0.85363
MAPE: 35.66418

Correlations
Pearson: 0.65115
Spearman: 0.59001
Distance: 0.34885
