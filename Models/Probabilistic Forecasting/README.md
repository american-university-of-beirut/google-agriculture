## Data sets

We currently have two data sets under 

* Daily Manual: The sheet that contains the manual data by applying the manual bowen correction method. It is named as All_Manual_Daily_Albedo_NDVI_LST_Cleaned.csv
<br />
* Daily Library: The sheet that contains the library data by applying the bowen and ebr correction methods using the flux qaqc library. It is named as All_Library_Daily_Albedo_NDVI_LST_Cleaned.csv

## Code

## Experiments


### Daily 
The folders represent the necessary outputs (plots, datasets, txt files) which are the result of the daily experiments. There are 4 folders for each
and the excel sheet contains all scores

#### Shallow 
The experiments are divided into 2: Smogn vs no Smogn.  
  
In each smogn/no_smogn folder there are:

    * Lib_All_Sites: Library dataset with all sites - with Smogn and without Smogn on combinations of SWIN/encoded/weather_data/lags
    * Lib_Ameri: Library dataset with Ameriflux sites - with Smogn and without Smogn on combinations of SWIN/encoded/weather_data/lags
    * Manual_All_Sites: Manual dataset with all sites - with Smogn and without Smogn on combinations of SWIN/encoded/weather_data/lags
    * Manual_Ameri: Manual dataset with Ameriflux sites - with Smogn and without Smogn on combinations of SWIN/encoded/weather_data/lags
    
#### BDL 
    * Base: Base parameters as in only weather and lags - for library and manual datasets
    * Encoded: Base parameters + encoded columns  - for library and manual datasets
    * SWIN: contains Base parameters + SWI_N - for library and manual datasets
    * SW_IN_Encoded: contains Base parameters, SWIN, and encoded - for library and manual datasets

#### Rare Study
Contains graphs (box cox, bar graph, density plot) to decide on rare ranges for both Library and Manual Datasets 

### Joint 
The folders represent the necessary outputs (plots, datasets, txt files) which are the result of the joint experiments. There are 4 folders for each
and the excel sheet contains all scores
#### Shallow 
Different experiments are performed under the Library All and the manual All for ET and ETo.  
  
On Library Dataset 4 experiments were performed.:

    * We have four variants: 
        ** Library_Base with no SWIN & no encoded columns
        ** Library_SWIN with SWIN and base columns
        ** Library_encoded with encoded and base columns
        ** Library_SWIN_encoded with SWIN, encoded, and base columns  
          
Each variant is tested against 6 types of residuals:

    * residual ET
    * squared residual ET
    * relative residual ET
    * residual ETo
    * squared residual ETo
    * relative residual ETo  
    
On Manual Dataset: 12 experiments were performed.

    * We have four variants: 
        ** Manual_Base with no SWIN & no encoded columns
        ** Manual_SWIN with SWIN and base columns
        ** Manual_encoded with encoded and base columns
        ** Manual_SWIN_encoded with SWIN, encoded, and base columns.   
        
Each variant is tried against 3 types of residuals since we do not have ET in the Manual Dataset: 

    * residual ETo
    * squared residual ETo
    * relative residual ETo 

#### BDL 
Different experiments are performed under the Library All and the manual All for ET and ETo

    * EEfluxET_ET: Study having input EEflux ET and lags - for library dataset
	* EEfluxET_LE: Study having input EEflux ET and lags - for manual dataset
	* EEfluxETo_ETo: Study having input EEflux ETo and lags - for library and manual datasets
    * Base: Base parameters as in only weather and lags - for library and manual datasets
    * Encoded: Base parameters + encoded columns  - for library and manual datasets
    * SWIN: contains Base parameters + SWI_N - for library and manual datasets
    * SW_IN_Encoded: contains Base parameters, SWIN, and encoded - for library and manual datasets
    * Encoded_Residual: has the residual (between ETo and EEflux ETo) as being the output - for library data set
    * Encoded_StandardizedResidual: has the standardized residual as being the output - for library data set
    * Encoded_SquredResidual: has the squared residual as being the output - for library data set
    * Encoded_RelativeResidual: has the ratio residual as being the output - for library data set

#### Study
Contains studying different correlation mesures, residual types, generating box plots, probablity distribution and different fitting algorithms
to study the relationship between EEflux ET/ETo and ET/ETo 