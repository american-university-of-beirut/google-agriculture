
Utility Based Metrics
F1: 0.68773
F2: 0.72093
F05: 0.65745
precision: 0.63870
recall: 0.74491

Regression Error Metrics
R2: 0.50657
Adj-R2: 0.48145
RMSE: 1.60267
MSE: 2.56854
MAE: 1.19373
MAPE: 40.34058

Correlations
Pearson: 0.71416
Spearman: 0.73461
Distance: 0.28584
